import { CUSTOM_ELEMENTS_SCHEMA, NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { GradeSubmissionsHomeComponent } from './components/grade-submissions-home/grade-submissions-home.component';
import { GradeSubmissionsRoutingModule } from './grade-submissions.routing';
import { GradeSubmissionsSharedModule } from './grade-submissions.shared';
import { GradeSubmissionsTableComponent } from './components/grade-submissions-table/grade-submissions-table.component';
import { TablesModule } from '@universis/ngx-tables';
import { TranslateModule } from '@ngx-translate/core';
import { FormsModule } from '@angular/forms';
import { SharedModule } from '@universis/common';
import { StudentsSharedModule } from '../students/students.shared';
import { InstructorsSharedModule } from '../instructors/instructors.shared';
import { ElementsModule } from '../elements/elements.module';
import {MostModule} from '@themost/angular';
import { BsDatepickerModule } from 'ngx-bootstrap';
import { DatePipe } from '@angular/common';
import { RegistrarSharedModule } from '../registrar-shared/registrar-shared.module';
import { AdvancedFormsModule } from '@universis/forms';
import {TooltipModule} from 'ngx-bootstrap';
import {ChartsModule} from 'ng2-charts';
import { CoursesSharedModule } from '../courses/courses.shared';
import { ReportsSharedModule } from '../reports-shared/reports-shared.module';
import {RouterModalModule} from '@universis/common/routing';
import { RequestsModule } from '../requests/requests.module';

@NgModule({
  imports: [
    CommonModule,
    TranslateModule,
    GradeSubmissionsSharedModule,
    TablesModule,
    GradeSubmissionsRoutingModule,
    FormsModule,
    SharedModule,
    StudentsSharedModule,
    InstructorsSharedModule,
    ElementsModule,
    MostModule,
    BsDatepickerModule.forRoot(),
    AdvancedFormsModule,
    RegistrarSharedModule,
    RouterModalModule,
    TooltipModule.forRoot(),
    ChartsModule,
    CoursesSharedModule,
    ReportsSharedModule,
    RequestsModule
  ],
  providers: [DatePipe],
  declarations: [
    GradeSubmissionsHomeComponent,
    GradeSubmissionsTableComponent,
    ],
  schemas: [CUSTOM_ELEMENTS_SCHEMA]
})
export class GradeSubmissionsModule { }
