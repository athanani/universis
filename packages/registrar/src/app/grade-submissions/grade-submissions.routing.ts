import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { GradeSubmissionsHomeComponent } from './components/grade-submissions-home/grade-submissions-home.component';
import { GradeSubmissionsTableComponent } from './components/grade-submissions-table/grade-submissions-table.component';
import { CurrentAcademicYearResolver } from '../registrar-shared/services/activeDepartmentService.service';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../registrar-shared/table-configuration.resolvers';

const routes: Routes = [
  {
    path: '',
    component: GradeSubmissionsHomeComponent,
    data: {
      title: 'GradeSubmissions'
    },
    children: [
      {
        path: '',
        pathMatch: 'full',
        redirectTo: 'list/pending'
      },
      {
        path: 'list/:list',
        component: GradeSubmissionsTableComponent,
        data: {
          title: 'grade-submissions List',
          model: 'ExamDocumentUploadActions'
        },
        resolve: {
          currentYear: CurrentAcademicYearResolver,
          tableConfiguration: TableConfigurationResolver,
          searchConfiguration: SearchConfigurationResolver
        }
      }
    ]
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  declarations: []
})
export class GradeSubmissionsRoutingModule {
}
