import { AfterViewInit, Component, ElementRef, OnInit, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { LoadingService, TemplatePipe } from '@universis/common';
import { BsModalRef } from 'ngx-bootstrap';
import { ActiveDepartmentService } from '../../../registrar-shared/services/activeDepartmentService.service';
import { Student } from '../../entities/student';
import { SearchService } from '../../search.service';
import * as SpotlightConfig from '../../search.config.list.json';
import { Instructor } from '../../entities/instructor';

@Component({
    selector: 'search-modal-content',
    templateUrl: './modal-content.component.html',
    styleUrls: ['./modal-content.component.scss']
})

export class ModalContentComponent implements AfterViewInit {
    @ViewChild('searchBox') searchElement: ElementRef;
    public students: Student[];
    public instructors: Instructor[];

    constructor(private searchService: SearchService,
        private _router: Router,
        private _context: AngularDataContext,
        public bsModalRef: BsModalRef,
        private _activeDepartmentService: ActiveDepartmentService,
        private _template: TemplatePipe,
        private _loadingService: LoadingService) { }

    ngAfterViewInit() {
        setTimeout(() => { // this will make the execution after the above boolean has changed
            this.searchElement.nativeElement.focus();
        }, 0);
    }
    async search(term: string) {
        try {
            this._loadingService.showLoading();
            const studentCallback = (student: Student) => {
                return {
                    studentIdentifier: student.studentIdentifier,
                    familyName: student.familyName,
                    givenName: student.givenName,
                    department: student.departmentName,
                };
            };
            const instructorCallback = (instructor: Instructor) => {
                return {
                    familyName: instructor.familyName,
                    givenName: instructor.givenName,
                    department: instructor.departmentName,
                    category: instructor.category
                };
            };
            const [students, instructors] = await Promise.all(
                [this.searchService.searchEntity<Student>(term, 'Students', studentCallback),
                this.searchService.searchEntity<Instructor>(term, 'Instructors', instructorCallback)
                ]);
            this.students = students;
            this.instructors = instructors;
            this._loadingService.hideLoading();
        } catch (err) {
            this._loadingService.hideLoading();
            console.error(err);
        }
    }

    moreStudentInfo() {
        this.bsModalRef.hide();
        this._router.navigate(['/students/list/active']);
    }

    moreInstructorInfo() {
        this.bsModalRef.hide();
        this._router.navigate(['/instructors/list']);
    }

    async goToStudent(entity: Student) {
        // Get current active department
        const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
        if (entity.departmentId !== activeDepartment.id) {
            // Get department of entity
            const newDepartment = await this._context.model('Departments').where('id').equal(entity.departmentId).getItem();
            this._activeDepartmentService.setActiveDepartment(newDepartment);
        }
        this.getConfigLink(entity);
        this.bsModalRef.hide();
    }

    goToInstructor(entity: Instructor) {
        this.getConfigLink(entity);
        this.bsModalRef.hide();
    }

    getConfigLink(entity: any) {
        // Get entity config redirect link
        const config = JSON.parse(JSON.stringify(SpotlightConfig));
        let configLink: any;
        config.forEach(element => {
            if (element.entitySet === entity.type) {
                configLink = element;
            }
        });
        const url = this._template.transform(configLink.link, {
            id: entity.id
        });
        // Reload current route
        setTimeout(() => {
            this._router.navigateByUrl(url).then(() => { });
        }, 500);
    }
}
