import { AfterViewInit, Component, OnDestroy, ViewChild } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { DIALOG_BUTTONS, ErrorService, LoadingService, ModalService } from '@universis/common';
import { AdvancedTableComponent, AdvancedTableConfiguration, AdvancedTableDataResult } from '@universis/ngx-tables';
import { AppEventService } from '@universis/common';
import { from, Observable, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';
import { TranslateService } from '@ngx-translate/core';
import { TableConfigurationResolver } from '../../../registrar-shared/table-configuration.resolvers';
import { ActiveFieldGroupService } from '../../active-field-group.service';
import { ReadonlyAccessResolver } from '../../resolvers';

@Component({
  selector: 'app-field-group-overview',
  templateUrl: './field-group-overview.component.html',
  providers: [
    ReadonlyAccessResolver
  ]
})
export class FieldGroupOverviewComponent implements AfterViewInit, OnDestroy {
  subscription: Subscription;
  group$?: Observable<any>;
  lastError: Error;
  recordsTotal?: number;
  readonly = false;

  @ViewChild('table') table: AdvancedTableComponent;

  constructor(private context: AngularDataContext,
    private activateRoute: ActivatedRoute,
    private loading: LoadingService,
    private router: Router,
    private event: AppEventService,
    private modal: ModalService,
    private translate: TranslateService,
    private errorService: ErrorService,
    private resolver: TableConfigurationResolver,
    private activeGroup: ActiveFieldGroupService,
    private readonlyAccess: ReadonlyAccessResolver) { }
  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
  ngAfterViewInit(): void {
    this.subscription = this.activateRoute.params.subscribe(({id}) => {
      this.loading.showLoading();
      this.lastError = null;
      this.group$ = from(
        this.context.model('ConsentFieldGroups').where('id').equal(id).getItem()
      );
      this.group$.subscribe((group?: { id: string, name: string, alternateName: string }) => {
        this.loading.hideLoading();
        this.activeGroup.group.next(group);
        this.readonlyAccess.resolve(this.activateRoute.snapshot, this.router.routerState.snapshot).pipe(first()).subscribe((readonly: boolean) => {
          this.readonly = readonly;
          this.resolver.get('ConsentFields', readonly ? 'readonly' : 'index').subscribe((tableConfiguration) => {
            const config = AdvancedTableConfiguration.cast(tableConfiguration);
            config.model = 'ConsentFields';
            config.defaults = {
              filter:`group eq '${group.alternateName}'`
            }
            this.table.config = config;
            this.table.fetch(true);
          });
        });
      }, ((error: Error) => {
        console.error(error);
        this.loading.hideLoading();
        this.lastError = error;
      }), () => {
        this.loading.hideLoading();
      });
    });

    this.event.changed.subscribe((event) => {
      if (event.model === 'ConsentFieldGroups') {
        this.table.fetch(true);
      }
    });

  }
  /**
   * Data load event handler of advanced table component
   * @param data
   */
  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  edit() {
    this.router.navigate([
      {
        outlets: {
          modal: [
            'edit'
          ]
        }
      }
    ], {
      relativeTo: this.activateRoute,
      skipLocationChange: true,
      replaceUrl: false
    });
  }

  ngOnInit() {
  }

  remove() {
    const table = this.table;
    if (table && table.selected && table.selected.length) {
      // get items to remove
      const items = table.selected;
      return this.modal.showWarningDialog(
        this.translate.instant('Tables.RemoveItemsTitle'),
        this.translate.instant('Tables.RemoveItemsMessage'),
        DIALOG_BUTTONS.OkCancel).then(result => {
          if (result === 'ok') {
            this.loading.showLoading();
            this.context.model(table.config.model).remove(items).then(() => {
              this.loading.hideLoading();
              table.fetch(true);
            }).catch(err => {
              this.loading.hideLoading();
              this.errorService.showError(err, {
                continueLink: '.'
              });
            });
          }
        });
    }
  }

}
