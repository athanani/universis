import {Component, OnInit, ViewChild, OnDestroy, Output, EventEmitter} from '@angular/core';
import { AdvancedTableComponent, AdvancedTableDataResult, TableColumnConfiguration } from '@universis/ngx-tables';
import { Router, ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import {Observable, Subscription} from 'rxjs';
import { AdvancedSearchFormComponent } from '@universis/ngx-tables';
import {AdvancedTableSearchComponent} from '@universis/ngx-tables';
import {ActivatedTableService} from '@universis/ngx-tables';
import {TranslateService} from '@ngx-translate/core';
import {ErrorService, LoadingService, ModalService} from '@universis/common';
import {AdvancedRowActionComponent} from '@universis/ngx-tables';
import {ClientDataQueryable} from '@themost/client';
import {COURSE_SHARED_COLUMNS as courseSharedColumns} from '../../../courses/components/courses-table/courses-shared-columns-config' ;

@Component({
  selector: 'app-exams-table',
  templateUrl: './exams-table.component.html',
})
export class ExamsTableComponent implements OnInit, OnDestroy {

  private dataSubscription: Subscription;
  public recordsTotal: any;
  private selectedItems: any;
  private examPeriod: any;
  private takeSize = 100;
  public isLoading: boolean = true;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  @Output() refreshAction: EventEmitter<any> = new EventEmitter<any>();

  constructor(private _router: Router,
              private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private _translateService: TranslateService,
              private _modalService: ModalService,
              private _loadingService: LoadingService,
              private _errorService: ErrorService,
              private _activatedTable: ActivatedTableService) {
  }

  ngOnInit() {
    this.dataSubscription = this._activatedRoute.data.subscribe( data => {
      this._activatedTable.activeTable = this.table;
      this.isLoading = true;
      // set search form
      if (data.searchConfiguration) {
        this.search.form = data.searchConfiguration;
        this.search.ngOnInit();
      }
      // set table config and recall data
      if (data.tableConfiguration) {
        // set config
        this.table.config = data.tableConfiguration;
        // reset search text
        this.advancedSearch.text = null;
        // reset table
        this.table.reset(false);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
    this.isLoading = false;
  }

  ngOnDestroy(): void {
    if (this.dataSubscription) {
      this.dataSubscription.unsubscribe();
    }
  }

  onSearchKeyDown(event: any) {
    if (event.keyCode === 13) {
      this.table.search((<HTMLInputElement>event.target).value);
      return false;
    }
  }

  onSelectBoxChange(event: any) {
    // get value
    const value = parseInt((<HTMLSelectElement>event.target).value, 10);
    if (isNaN(value)) {
      this._router.navigate(
        [],
        {
          relativeTo: this._activatedRoute,
          queryParams: { $filter: '' },
          queryParamsHandling: 'merge'
        }).then(() => {
          this.table.fetch();
        });
      return;
    }
    this._router.navigate(
      [],
      {
        relativeTo: this._activatedRoute,
        queryParams: { $filter: `status eq ${value}` },
        queryParamsHandling: 'merge'
      }).then(() => {
        this.table.fetch();
      });
  }


  /**
   * Executes open action for course classes
   */

  executeChangeStatusAction(status) {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id,
          status: {
            alternateName: status
          }
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseExams').save(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }

  async openAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      const promises = items
        .filter(item => item.status === 'closed')
        .map(async item => {
          return {
            id: item.id,
            status: item.status,
            hasActiveSubmissions: await this.validateExam(item)
          };
        });
      this.selectedItems = (await Promise.all(promises)).filter(item => !item.hasActiveSubmissions);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.OpenAction.Title',
          description: 'Exams.OpenAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('open')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }

  async closeAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      const promises = items
        .filter(item => item.status === 'open' || item.status === 'completed')
        .map(async item => {
          return {
            id: item.id,
            status: item.status,
            hasActiveSubmissions: await this.validateExam(item)
          };
        });
      this.selectedItems = (await Promise.all(promises)).filter(exam => !exam.hasActiveSubmissions);
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.CloseAction.Title',
          description: 'Exams.CloseAction.Description',
          refresh: this.refreshAction,
          execute: this.executeChangeStatusAction('closed')
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Deletes selected course exams
   */
  async deleteAction() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items without students
      this.selectedItems = items.filter( (item) => {
        return (!item.numberOfGradedStudents || item.numberOfGradedStudents === 0) && item.status !== 'completed';
      });
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.DeleteAction.Title',
          description: 'Exams.DeleteAction.Description',
          refresh: this.refreshAction,
          execute: this.executeDeleteAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes copy action for course classes
   */
  executeDeleteAction() {
    return new Observable((observer) => {
      this.refreshAction.emit({
        progress: 1
      });
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // execute update for all items (transactional update)
      // map items
      const updated = this.selectedItems.map((item) => {
        return {
          id: item.id
        };
      });
      // handle fake progress with interval
      let progressValue = 5;
      const progressInterval = setInterval(() => {
        progressValue = progressValue + 10 < 100 ? progressValue + 10 : 5;
        this.refreshAction.emit({
          progress: progressValue
        });
      }, 1000);
      this._context.model('CourseExams').remove(updated).then(() => {
        // reload table
        this.table.fetch(true);
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result
        return observer.next(result);
      }).catch((err) => {
        // stop progress
        if (progressInterval) {
          clearInterval(progressInterval);
        }
        // and return result with errors
        result.errors = result.total;
        return observer.next(result);
      });
    });
  }
  /**
   * Add result date to selected course exams
   */
  async AddResultDateAction() {
    try {
    let message = this._translateService.instant('Exams.AddResultDateMessage.NoItems');
    this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      // get only items without students
      this.selectedItems = items;
      const allSameExamPeriodAndYear = this.selectedItems.length > 0 &&
        this.selectedItems.every(item =>
          item.examPeriod === this.selectedItems[0].examPeriod &&
          item.year === this.selectedItems[0].year
        );
      if (!allSameExamPeriodAndYear) {
        message = this._translateService.instant('Exams.AddResultDateMessage.NoItems');
        this.selectedItems = [];
      }
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          formTemplate: this.selectedItems && this.selectedItems.length > 0 ? 'CourseExams/updateExamResultDate' : null,
          modalTitle: 'Exams.AddResultDateMessage.Title',
          description: 'Exams.AddResultDateMessage.Description',
          additionalMessage:  message,
          refresh: this.refreshAction,
          execute: this.executeAddResultDateAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes Add result date to selected course exams
   */
  executeAddResultDateAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      // get values from modal component
      const component = this._modalService.modalRef.content as AdvancedRowActionComponent;
      const data = component.formComponent.form.formio.data;
      this.refreshAction.emit({
        progress: 1
      });

      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // load document request

            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            const action = await this._context.model(`CourseExams/${item.id}`).save(data);
            result.success += 1;
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  /**
   * Calculates student statistics for selected course classes
   */
  async calculateStatistics() {
    try {
      this._loadingService.showLoading();
      const items = await this.getSelectedItems();
      this.selectedItems = items;
      this._loadingService.hideLoading();
      this._modalService.openModalComponent(AdvancedRowActionComponent, {
        class: 'modal-lg',
        keyboard: false,
        ignoreBackdropClick: true,
        initialState: {
          items: this.selectedItems,
          modalTitle: 'Exams.CalculateStatisticsAction.Title',
          description: 'Exams.CalculateStatisticsAction.Description',
          refresh: this.refreshAction,
          execute: this.executeCalculateStatisticsAction()
        }
      });
    } catch (err) {
      this._loadingService.hideLoading();
      this._errorService.showError(err, {
        continueLink: '.'
      });
    }
  }
  /**
   * Executes calculate statistics action for course classes
   */
  executeCalculateStatisticsAction() {
    return new Observable((observer) => {
      const total = this.selectedItems.length;
      const result = {
        total: this.selectedItems.length,
        success: 0,
        errors: 0
      };
      this.refreshAction.emit({
        progress: 1
      });
      // execute promises in series within an async method
      (async () => {
        for (let index = 0; index < this.selectedItems.length; index++) {
          try {
            const item = this.selectedItems[index];
            // set progress
            this.refreshAction.emit({
              progress: Math.floor(((index + 1) / total) * 100)
            });
            await this._context.model(`CourseExams/${item.id}/calculateStatistics`).save(null);
            result.success += 1;
            // do not throw error while updating row
            // (user may refresh view)
            try {
              await this.table.fetchOne({
                id: item.id
              });
            } catch (err) {
              //
            }
          } catch (err) {
            // log error
            console.log(err);
            result.errors += 1;
          }
        }
      })().then(() => {
        observer.next(result);
      }).catch((err) => {
        observer.error(err);
      });
    });
  }


  async getSelectedItems() {
    let items = [];
    if (this.table && this.table.lastQuery) {
      const lastQuery: ClientDataQueryable = this.table.lastQuery;
      if (lastQuery != null) {
        if (this.table.smartSelect) {
          // get items
          const selectArguments = ['id', 'status/alternateName as status', 'year/id as year', 'examPeriod', 'numberOfGradedStudents'];
          // query items
          const queryItems = await lastQuery.select.apply(lastQuery, selectArguments)
            .take(-1)
            .skip(0)
            .getItems();
          if (this.table.unselected && this.table.unselected.length) {
            // remove items that have been unselected by the user
            items = queryItems.filter( item => {
              return this.table.unselected.findIndex( (x) => {
                return x.id === item.id;
              }) < 0;
            });
          } else {
            items = queryItems;
          }
        } else {
          // get selected items only
          items = this.table.selected.map( (item) => {
            return {
              id: item.id,
              status: item.status,
              year: item.year.id,
              examPeriod: item.examPeriod,
              numberOfGradedStudents: item.numberOfGradedStudents
            };
          });
        }
      }
    }
    return items;
  }
  private async validateExam(exam): Promise<number> {
    const gradeSubmissions = await this._context.model('CourseExams/' + exam.id + '/actions')
      .select('count(id) as total')
      .where('actionStatus/alternateName').equal('ActiveActionStatus')
      .and('additionalResult').notEqual(null)
      .getItem();
    return gradeSubmissions && gradeSubmissions.total;
  }


  onLoading(event: { target: AdvancedTableComponent }) {
    if (event && event.target && event.target.config) {
      // clone shared columns
      const clonedSharedColumns = JSON.parse(JSON.stringify(courseSharedColumns));
      const configColumns = event.target.config.columns || [];
      clonedSharedColumns.forEach((column: TableColumnConfiguration) => {
        // adapt name
        column.name = `course/${column.name}`;
        // try to find column in the config by name
        const findColumn = configColumns.find((configColumn) => configColumn.name === column.name);
        // if it does not exist
        if (findColumn == null) {
          // push it
          configColumns.push(column);
        }
      });
    }
  }


}
