import { Component, Input, OnDestroy, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { Subscription } from 'rxjs/Subscription';

@Component({
  selector: 'app-internships-preview',
  templateUrl: './internships-preview.component.html'
})
export class InternshipsPreviewComponent implements OnInit, OnDestroy {

  @Input() model: any;
  private subscription: Subscription;
  public isLoading = true;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
  }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.isLoading = true;
      this.model = await this._context.model('Internships')
        .where('id').equal(params.id)
        .expand('status,internshipPeriod,department,info,student($expand=person)')
        .getItem();

      this.isLoading = false;
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

}
