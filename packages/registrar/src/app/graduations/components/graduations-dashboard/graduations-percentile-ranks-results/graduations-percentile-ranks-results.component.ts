import { Component, OnDestroy, OnInit, ViewChild } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
// tslint:disable-next-line: max-line-length
import {
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult,
  AdvancedTableSearchComponent,
} from '@universis/ngx-tables';
import { Subscription } from 'rxjs';
import * as PERCENTILE_RANKS_RESULTS_CONFIG from './percentile-ranks-results.config.list.json';
import * as PERCENTILE_RANKS_RESULTS_SEARCH_CONFIG from './percentile-ranks-results.search.list.json';

@Component({
  selector: 'app-graduations-percentile-ranks-results',
  templateUrl: './graduations-percentile-ranks-results.component.html',
  styleUrls: ['./graduations-percentile-ranks-results.component.scss'],
})
export class GraduationsPercentileRanksResultsComponent
  implements OnInit, OnDestroy {
  public recordsTotal: number | string;
  public tableConfiguration: any;
  public searchConfiguration: any;
  @ViewChild('table') table: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  @ViewChild('advancedSearch') advancedSearch: AdvancedTableSearchComponent;
  private _paramsSubscription: Subscription;
  public graduationEventId: any;
  constructor(
    private readonly _context: AngularDataContext,
    private readonly _activatedRoute: ActivatedRoute
  ) {}

  ngOnInit() {
    this._paramsSubscription = this._activatedRoute.params.subscribe(
      (params) => {
        this.graduationEventId = params.id;
        // set search config
        this.searchConfiguration = PERCENTILE_RANKS_RESULTS_SEARCH_CONFIG;
        this.search.form = this.searchConfiguration;
        // pass graduation event to form
        Object.assign(this.search.form, {
          graduationEvent: params.id,
        });
        // and init
        this.search.ngOnInit();
        // prepare table query
        this.table.query = this._context
          .model('PercentileRankActionResults')
          .where('action/targetType')
          .equal('GraduationEvent')
          .and('action/targetIdentifier')
          .equal(params.id.toString())
          .and('action/rankType/alternateName').equal('coursePercentileRank')
          .orderByDescending('action/dateCreated')
          .thenByDescending('dateCreated')
          .prepare();
        // set table config
        this.tableConfiguration = PERCENTILE_RANKS_RESULTS_CONFIG;
        this.table.config = AdvancedTableConfiguration.cast(
          this.tableConfiguration
        );
        // reset table
        this.table.ngOnInit();
      }
    );
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this._paramsSubscription) {
      this._paramsSubscription.unsubscribe();
    }
  }
}
