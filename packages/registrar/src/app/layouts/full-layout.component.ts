import {Component, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { AngularDataContext } from '@themost/angular';
import {ActivatedUser, AppSidebarService, AuthGuard, UserStorageService} from '@universis/common';
import {ConfigurationService, UserService} from '@universis/common';
import {ActivatedRoute, Router} from '@angular/router';
import {ActiveDepartmentService} from '../registrar-shared/services/activeDepartmentService.service';
import {Subscription} from 'rxjs/Subscription';
import {UserDepartmentsService} from '../registrar-shared/services/userDepartments.service';
import { LocalizationSettingsConfiguration } from '@universis/common';
import { Title } from '@angular/platform-browser';

@Component({
  selector: 'app-dashboard',
  templateUrl: './full-layout.component.html',
  styles: [
      `
      header.app-header.navbar {
        justify-content: unset;
      }

      button.dropdown-item {
        cursor: pointer;
      }

      input.form-search {
        outline: none;
      }

      .departement-nav{
        text-align: left;
      }

      .departement-nav .navbar-nav-content {
        position: absolute;
        width: auto;
        min-width: 240px;
        max-width: 500px;
        top: 100%;
        overflow-y: initial;
        max-height: initial;
        transform: translateY(0);
        box-shadow: 0 4px 17px 0 rgba(177,177,177,0.5);
      }

      .departement-nav .departement-nav-sidebar {
        width: 30%;
      }

      .departement-nav .departement-nav-content {
        width: 70%;
      }

      .departement-nav .tab-content {
        display: flex;
        flex-direction: row;
        width: 500px;
        min-height: 380px;
        overflow: hidden;
      }

      .departement-nav .tab-content .tab-pane {
        display: none;
      }

      .departement-nav .tab-content .tab-pane.active {
        display: block;
      }

      .departement-nav .tab-content, .nav-tabs {
        border: 0;
      }

      .departement-nav .tab-pane a {
        cursor: pointer;
      }

      .departement-nav .dropdown-menu .divider {
        width: 100%;
        margin: 5px 1px;
        overflow: hidden;
        border-bottom: 1px solid #e8e1e1;
      }

      .departement-nav ul.nav.nav-tabs > li > a.active {
        font-weight: bold;
      }

      .departement-nav .border-right {
        border-right: 1px solid #f0f3f5 !important;
      }

      .departement-nav .close {
        opacity: 1;
        position: absolute;
        top: 10px;
        right: 10px;
        z-index: 9;
      }

      @media screen and (max-width: 575.98px) {
        .departement-nav .navbar-nav-content {
          margin-left: 15px;
          margin-right: 15px;
        }

        .departement-nav .tab-content {
          flex-direction: column;
          height: auto;
          flex: 1;
        }

        .departement-nav .departement-nav-sidebar {
          border-bottom: 1px solid #f0f3f5 !important;
        }

        .departement-nav .tab-content,
        .departement-nav .departement-nav-sidebar,
        .departement-nav .departement-nav-content{
          width: 100%;
        }

        .departement-nav .nav-item {
          position: inherit;
        }

        .departement-nav,
        .departement-nav > li > a {
            max-width: 300px;
        }

        @media screen and (min-width: 992px) {
            .departement-nav,
            .departement-nav > li > a {
                max-width: 420px;
            }
        }
      }
    `
  ]
})
export class FullLayoutComponent implements OnInit, OnDestroy {
  private userSubscription: Subscription;
  applicationTitle: any;


  constructor(private _context: AngularDataContext,
              private _configurationService: ConfigurationService,
              private _userService: UserService,
              private _activatedRoute: ActivatedRoute,
              private _appSidebarService: AppSidebarService,
              private _activeDepartmentService: ActiveDepartmentService,
              private _router: Router,
              private _userDepartments: UserDepartmentsService,
              private _userStorage: UserStorageService,
              private _activatedUser: ActivatedUser,
              private _authGuard: AuthGuard,
              private _titleService: Title) {
  }

  @ViewChild('appSidebar') appSidebar: any;
  @ViewChild('appSidebarNav') appSidebarNav: any;
  public status = { isOpen: false };
  public user;
  public today = new Date();
  public currentLang;
  public languages;
  public model: any;
  public activeDepartment: any;
  public _activeDepartmentSource: Subscription;
  public search = [];
  public isCollapsed = true;
  public applicationImage;

  async ngOnInit() {

    // register for activated user
    this.userSubscription = this._activatedUser.user.subscribe((user: any) => {
      if (user != null) {
        try {
          // enumerate sidebar navigation items
          this._appSidebarService.navigationItems.forEach((item) => {
            const canActivate = this._authGuard.canActivateLocation(item.url, user);
            if (canActivate == null) {
              if (item.children) {
                let canActivateOneChild = false;
                // enumerate children
                item.children.forEach((child) => {
                  // can activate child ?
                  const canActivateChild = this._authGuard.canActivateLocation(child.url, user);
                  if (canActivateChild && (canActivateChild.mask & 1) === 1) {
                    canActivateOneChild = true;
                  } else {
                    // hide child
                    child.class = 'd-none';
                  }
                });
                if (canActivateOneChild === false) {
                  // hide parent also
                  item.class = 'd-none';
                }
              } else {
                // hide item (no children)
                item.class = 'd-none';
              }
            } else if ((canActivate.mask & 1) === 0) {
              // hide item
              item.class = 'd-none';
            }
          });
          // and finally set sidebar items
          this.appSidebarNav.navItems = this._appSidebarService.navigationItems;
        } catch (err) {
          console.error('FullLayoutComponent', 'An error occured while preparing application sidebar.');
          console.error('FullLayoutComponent', err);
        }

      }
    }, (err) => {
      console.error('FullLayoutComponent', 'An error occured while getting activated user.');
      console.error('FullLayoutComponent', err);
    });
    // get minimized option from user storage
    const minimized = await this._userStorage.getItem('registrar/sidebar/minimized');
    if (minimized != null) {
      // set minimized
      this.appSidebar.minimized = minimized.value;
      // and call method to minimize sidebar
      this.appSidebar.isMinimized(this.appSidebar.minimized);
    }
    // get current user
    this.user = await this._userService.getUser();
    // get current language
    this.currentLang = this._configurationService.currentLocale;
    // get languages
    const localizationSettings: LocalizationSettingsConfiguration | any = this._configurationService.settings.i18n;
    if (Array.isArray(localizationSettings.uiLocales)) {
      this.languages = localizationSettings.uiLocales;
    } else {
      this.languages = localizationSettings.locales;
    }

    this.model = await this._userDepartments.getDepartments();

      // Source object with binding for active department
    this._activeDepartmentSource = this._activeDepartmentService.departmentChange
      .subscribe(activeDepartment => this.activeDepartment = activeDepartment);

    // local object for active department
    this.activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    if (!this.activeDepartment) {
      if (this.model && this.model.length > 0) {
        this.activeDepartment = this.model[0];
      }
    }
    // get path of brand logo
    this.applicationImage = this._configurationService.settings.app && this._configurationService.settings.app.image;
    this.applicationTitle = this._titleService.getTitle();
  }

  ngOnDestroy() {

    // prevent memory leak when component is destroyed
    if (this._activeDepartmentSource) {
      this._activeDepartmentSource.unsubscribe();
    }
    if (this.userSubscription) {
      this.userSubscription.unsubscribe();
    }
  }

  changeLanguage(lang) {
    this._configurationService.currentLocale = lang;
    // reload current route
    window.location.reload();
  }

  changeDepartment(department) {
    this.activeDepartment = department;
    this._activeDepartmentService.setActiveDepartment(this.activeDepartment);
    // reload current route
    setTimeout(() => {
        this._router.navigateByUrl(`/`).then(
          () => {  window.location.reload(); });
      },
      500);
  }

  onMinimizeSidebar(event: any) {
    // if event source element is sidebar-minimizer
    if (event.srcElement.className === 'sidebar-minimizer') {
      // query minimized from body class
      const minimized = document.querySelector('body').classList.contains('sidebar-minimized');
      // set user storage value
      this._userStorage.setItem('registrar/sidebar/minimized', minimized).then(() => {
        // do nothing
      });
    }
  }
}
