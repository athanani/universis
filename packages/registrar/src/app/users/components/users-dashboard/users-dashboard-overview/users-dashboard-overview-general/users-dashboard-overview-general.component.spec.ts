import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UsersDashboardOverviewGeneralComponent } from './users-dashboard-overview-general.component';

describe('UsersDashboardOverviewGeneralComponent', () => {
  let component: UsersDashboardOverviewGeneralComponent;
  let fixture: ComponentFixture<UsersDashboardOverviewGeneralComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UsersDashboardOverviewGeneralComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UsersDashboardOverviewGeneralComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
