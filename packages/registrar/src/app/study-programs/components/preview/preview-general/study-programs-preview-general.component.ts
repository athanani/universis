import {Component, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {ActivatedRoute} from '@angular/router';
import {Subscription} from 'rxjs';

@Component({
  selector: 'app-study-programs-preview-general',
  templateUrl: './study-programs-preview-general.component.html'
})
export class StudyProgramsPreviewGeneralComponent implements OnInit, OnDestroy {
  public model: any;
  private subscription: Subscription;
  public studyProgramID: any;
  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext) {
}

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studyProgramID = params.id;
      this.model = await this._context.model('StudyPrograms')
        .where('id').equal(this.studyProgramID)
        .expand('department,studyLevel')
        .getItem();
    });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
