import { Component, OnInit, Input } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { TranslateService } from '@ngx-translate/core';
import {ActiveDepartmentService} from '../../../registrar-shared/services/activeDepartmentService.service';
import { cloneDeep, template } from 'lodash';
import * as DEPARTMENTS_LIST_CONFIG from '../departments-table/departments-table.config.json';

@Component({
  selector: 'app-departments-root',
  templateUrl: './departments-root.component.html'
})
export class DepartmentsRootComponent implements OnInit {

  @Input() departments: any;
  public isCreate = false;
  public config: any;
  public actions: any[];
  public allowedActions: any[];
  public edit: any;

  constructor(private _activatedRoute: ActivatedRoute,
              private _context: AngularDataContext,
              private translate: TranslateService,
              private _activeDepartmentService: ActiveDepartmentService) {
  }

  async ngOnInit() {

    if (this._activatedRoute.snapshot.url.length > 0 &&
      this._activatedRoute.snapshot.url[0].path === 'create') {
      this.isCreate = true;
    } else {
      this.isCreate = false;
    }

    this.departments = await this._context.model('LocalDepartments')
      .where('id').equal(this._activatedRoute.snapshot.params.id)
      .getItem();

     // @ts-ignore
     this.config = cloneDeep(DEPARTMENTS_LIST_CONFIG as TableConfiguration);

     if (this.config.columns && this.departments) {
       // get actions from config file
       this.actions = this.config.columns.filter(x => {
         return x.actions;
       })
         // map actions
         .map(x => x.actions)
         // get list items
         .reduce((a, b) => b, 0);
 
       // filter actions with student permissions
       this.allowedActions = this.actions.filter(x => {
         if (x.role) {
           if (x.role === 'action') {
             return x;
           }
         }
       });
 
       this.edit = this.actions.find(x => {
         if (x.role === 'edit') {
           x.href = template(x.href)(this.departments);
           return x;
         }
       });
 
       this.actions = this.allowedActions;
       this.actions.forEach(action => {
         action.href = template(action.href)(this.departments);
       });
 
     }

    const activeDepartment = await this._activeDepartmentService.getActiveDepartment();
    if(this.departments.id === activeDepartment.id){
      if(!this._activeDepartmentService.checkDepartmentsEqual(activeDepartment, this.departments)){
        this._activeDepartmentService.setActiveDepartment(this.departments);
      }
    }
  }

}
