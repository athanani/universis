import {AfterViewInit, Component, Injectable, OnDestroy, OnInit, ViewChild} from '@angular/core';
import { ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot } from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { AdvancedListComponent } from '@universis/ngx-tables';
import * as DepartmentReportVariableValueConfiguration from '../../report-variables.config.json';
import { cloneDeep } from 'lodash';
@Component({
  selector: 'app-department-snapshot-variables',
  templateUrl: './department-snapshot-variables.component.html'
})
export class DepartmentSnapshotVariablesComponent implements OnInit, OnDestroy, AfterViewInit {
  model: any;
  paramSubscription: any;

  @ViewChild('list') list: AdvancedListComponent;
  tableConfiguration: typeof DepartmentReportVariableValueConfiguration;

  constructor(private activatedRoute: ActivatedRoute, private context: AngularDataContext) {
  }
  ngAfterViewInit(): void {
    //
  }
  ngOnDestroy(): void {
    if (this.paramSubscription) {
      this.paramSubscription.unsubscribe();
    }
  }

  ngOnInit() {
    this.paramSubscription = this.activatedRoute.params.subscribe((params) => {
      // clone table configuration
      const tableConfiguration: any = cloneDeep(DepartmentReportVariableValueConfiguration);
      // set default filter
      tableConfiguration.defaults.filter = `(department eq '${params.snapshot}')`;
      // and reset table configuration
      this.tableConfiguration = tableConfiguration;
      // get department snapshot  
      this.context.model('DepartmentSnapshots').where('id').equal(params.snapshot).getItem().then((item) => {
        this.model = item;
      });
    });
  }
}

@Injectable()
export class ActiveDepartmentSnapshotResolver implements Resolve<any> {
  constructor() {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any>|any {
    return route.params.snapshot;
  }
}