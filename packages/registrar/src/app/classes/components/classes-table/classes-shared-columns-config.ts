import { TableColumnConfiguration } from '@universis/ngx-tables';

export const CLASS_SHARED_COLUMNS: TableColumnConfiguration[] = [
  {
    name: 'id',
    property: 'classId',
    title: 'Classes.Shared.Id',
    hidden: true,
    optional: true
  },
  {
    name: 'title',
    property: 'classTitle',
    title: 'Classes.Shared.Title',
    hidden: true,
    optional: true
  },
  {
    name: 'year/alternateName',
    property: 'classAcademicYear',
    title: 'Classes.Shared.AcademicYear',
    hidden: true,
    optional: true
  },
  {
    name: 'period/name',
    property: 'classPeriod',
    title: 'Classes.Shared.Period',
    hidden: true,
    optional: true
  },
  {
    name: 'numberOfStudents',
    property: 'classNumberOfStudents',
    title: 'Classes.Shared.NumberOfStudents',
    hidden: true,
    optional: true
  },
  {
    name: 'statistic/examined',
    property: 'classExamined',
    title: 'Classes.Shared.NumberOfStudentsExamined',
    hidden: true,
    optional: true
  },
  {
    name: 'statistic/passed',
    property: 'classPassed',
    title: 'Classes.Shared.NumberOfStudentsPassed',
    hidden: true,
    optional: true
  },
  {
    name: 'statistic/hasRules',
    property: 'classHasRules',
    title: 'Classes.Shared.HasRules',
    formatter: 'TrueFalseFormatter',
    hidden: true,
    optional: true
  },
  {
    name: 'statistic/registrationType',
    property: 'classRegistrationType',
    title: 'Classes.Shared.RegistrationType',
    formatters: [
      {
        formatter: 'TranslationFormatter',
        formatString: 'RegistrationTypes.${value}'
      }
    ],
    hidden: true,
    optional: true
  },
  {
    name: 'weekHours',
    property: 'classWeekHours',
    title: 'Classes.Shared.WeekHours',
    hidden: true,
    optional: true
  },
  {
    name: 'totalHours',
    property: 'classTotalHours',
    title: 'Classes.Shared.TotalHours',
    hidden: true,
    optional: true
  },
  {
    name: 'status/alternateName',
    property: 'classStatus',
    title: 'Classes.ClassStatus',
    formatters: [
      {
        formatter: 'TranslationFormatter',
        formatString: 'ClassStatuses.${value}'
      }
    ],
    hidden: true,
    optional: true
  },
  {
    name: 'Instructors',
    property: 'classInstructors',
    virtual: true,
    sortable: false,
    title: 'Classes.Shared.Instructors',
    formatters: [
      {
        formatter: 'TemplateFormatter',
        formatString: '${instructors.length ? instructors.map(x => x.instructor && (x.instructor.familyName + " " + x.instructor.givenName)).join(", ") : "-"}'
      }
    ],
    hidden: true,
    optional: true
  },
  {
    name: 'maxNumberOfStudents',
    property: 'classMaxNumberOfStudents',
    title: 'Classes.Shared.MaxNumberOfStudents',
    hidden: true,
    optional: true
  },
  {
    name: 'minNumberOfStudents',
    property: 'classMinNumberOfStudents',
    title: 'Classes.Shared.MinNumberOfStudents',
    hidden: true,
    optional: true
  },
  {
    name: 'absenceLimit',
    property: 'classAbsenceLimit',
    title: 'Classes.Shared.AbsenceLimit',
    hidden: true,
    optional: true
  },
  {
    name: 'mustRegisterSection',
    property: 'classMustRegisterSection',
    title: 'Classes.MustRegisterSection',
    formatter: 'TrueFalseFormatter',
    hidden: true,
    optional: true
  }
]
