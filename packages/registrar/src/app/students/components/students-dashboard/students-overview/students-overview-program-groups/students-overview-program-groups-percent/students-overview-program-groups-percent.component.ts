import { Component, OnDestroy, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { TranslateService } from '@ngx-translate/core';
import { AngularDataContext } from '@themost/angular';
import {AppEventService, LoadingService} from '@universis/common';
import { RouterModalOkCancel } from '@universis/common/routing';
import { Subscription } from 'rxjs';

@Component({
  selector: 'app-students-overview-program-groups-percent',
  templateUrl: './students-overview-program-groups-percent.component.html'
})
export class StudentsOverviewProgramGroupsPercentComponent extends RouterModalOkCancel implements OnInit, OnDestroy {

  private subscription: Subscription;
  private studentId: any;
  public mode: any;
  public model: any;
  public lastError: any;

  constructor(protected _router: Router,
    protected _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _translateService: TranslateService,
    private _loadingService: LoadingService,
    private _appEvent: AppEventService
  ) {
    // call super constructor.
    super(_router, _activatedRoute);
  }

  ngOnInit() {
    this._loadingService.showLoading();
    // set up modal.
    this.modalTitle = this._translateService.instant('Students.Percentages');
    this.okButtonText = this._translateService.instant('OK');
    this.cancelButtonText = this._translateService.instant('Cancel');
    this.modalClass = 'modal-xl';
    this.subscription = this._activatedRoute.params.subscribe(async params => {
      this.studentId = this.activatedRoute.snapshot.parent.params.id;
      this.mode = this.activatedRoute.snapshot.routeConfig.path.split('/').slice(-1)[0];
      // fetch student courses.
      this.model = await this._context.model('StudentCourses')
        .where('programGroup').equal(params.id).and('student').equal(this.studentId)
        .expand('course')
        // tslint:disable-next-line: max-line-length
        .select('course/displayCode as displayCode, course/name as name, calculated, groupPercent, formattedGrade, isPassed, course, id')
        .take(-1)
        .getItems();
      this.okButtonDisabled = this.mode === 'edit' && !(this.percentagesAreValid());
      this._loadingService.hideLoading();
    });
  }

  async ok() {
    // clear last error.
    this.lastError = null;
    if (this.model.length && this.mode === 'edit') {
      this._loadingService.showLoading();
      // gather percentages to be saved.
      const percentagesToBeSaved = this.model.map(course => {
        return {
          id: course.id,
          course: course.course,
          student: this.studentId,
          groupPercent: parseFloat(course.groupPercent),
          calculated: course.calculated
        };
      });
      // save percentages.
      try {
        await this._context.model(`StudentCourses`).save(percentagesToBeSaved);
        // pass change event to reload programGroup
        this._appEvent.change.next({ model: 'StudentProgramGroups' });
        this._loadingService.hideLoading();
      } catch (err) {
        this._loadingService.hideLoading();
        this.lastError = err;
        return false;
      }
    }
    // close
    return this.close();
  }

  ngOnDestroy() {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }

  async cancel() {
    // close.
    return this.close();
  }

  percentagesAreValid() {
    if (!this.model.length) { return true; }
    let areSame = true;
    let sum = 0;
    let toCompare = parseFloat(this.model[0].groupPercent);

    for (const course of this.model) {
      const groupPercent = parseFloat(course.groupPercent);
      if (groupPercent < 0 || groupPercent > 1) { return false; }
      if (toCompare === 0) { toCompare = groupPercent; }
      if (areSame) { areSame = groupPercent === toCompare || groupPercent === 0; }
      sum += groupPercent;
    }
    toCompare = Math.round(toCompare * 100) / 100;
    return (areSame && (toCompare === 0 || toCompare === 1)) || sum === 1;
  }

  onChange() {
    if (this.model.length) {
      this.model.forEach(studentCourse => {
        if (studentCourse.calculated !== 1 && studentCourse.calculated !== true) {
          studentCourse.groupPercent = 0;
        }
      });
    }
    this.okButtonDisabled = !this.percentagesAreValid();
  }
}
