import {Component, Input, OnDestroy, OnInit} from '@angular/core';
import {AngularDataContext} from '@themost/angular';
import {Subscription} from 'rxjs';
import {ActivatedRoute} from '@angular/router';

@Component({
  selector: 'app-students-overview-internships',
  templateUrl: './students-overview-internships.component.html',
  styleUrls: ['./students-overview-internships.component.scss']
})
export class StudentsOverviewInternshipsComponent implements OnInit, OnDestroy  {

  public internships: any;
  @Input() studentId: number;
  private subscription: Subscription;

  constructor(private _context: AngularDataContext,
              private _activatedRoute: ActivatedRoute) { }

  async ngOnInit() {
    this.subscription = this._activatedRoute.params.subscribe(async (params) => {
      this.studentId = params.id;
    this.internships = await this._context.model('Internships')
      .asQueryable()
      .where('student').equal(this.studentId)
      .getItems();
   });
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
  }
}
