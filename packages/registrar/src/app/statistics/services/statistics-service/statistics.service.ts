import { Injectable } from '@angular/core';
import { AngularDataContext } from '@themost/angular';

@Injectable({
  providedIn: 'root'
})
export class StatisticsService {

  constructor(private _context: AngularDataContext) { }

  /**
   * 
   * Gets the reports templats of a certain category
   * 
   * @param {string} category The alternateName of the category
   * @returns {Array<any>} The report templates of the certain category
   * 
   */
  getReportTemplatesOfCategory(category: string): Promise<any[]> {
    return this._context.model('ReportTemplates')
      .where('reportCategory/alternateName')
      .equal(category)
      .take(-1)
      .getItems();
  }

  /**
   * 
   * Gets the reports categories that relates to statistics
   * 
   * @returns The list with all the report categories entries
   */
  getStatisticsReportCategories(): Promise<any[]> {
    return this._context.model('ReportCategories')
        .where('about')
        .equal('statistics')
        .take(-1)
        .getItems();
  }
}
