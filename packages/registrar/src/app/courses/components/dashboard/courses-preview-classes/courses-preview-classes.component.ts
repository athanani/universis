import {Component, Injectable, OnDestroy, OnInit, ViewChild} from '@angular/core';
import {
  AdvancedSearchFormComponent,
  AdvancedTableComponent,
  AdvancedTableConfiguration,
  AdvancedTableDataResult
} from '@universis/ngx-tables';
import {ActivatedRoute, ActivatedRouteSnapshot, Resolve, RouterStateSnapshot} from '@angular/router';
import { AngularDataContext } from '@themost/angular';
import { ActivatedTableService } from '@universis/ngx-tables';
import {Args} from '@themost/client';
import {AdvancedFormParentItemResolver} from '@universis/forms';
import {Subscription, combineLatest} from 'rxjs';
import { SearchConfigurationResolver, TableConfigurationResolver } from '../../../../registrar-shared/table-configuration.resolvers';
import { map } from 'rxjs/operators';

@Injectable({ providedIn: 'root' })
export class CourseTitleResolver implements Resolve<any> {
  constructor(private _context: AngularDataContext  ) {}

  resolve(
    route: ActivatedRouteSnapshot,
    state: RouterStateSnapshot
  ): Promise<any>|any {
  return  new AdvancedFormParentItemResolver(this._context).resolve(route, state).then(result => {
      return result.name;
    });
  }
}


@Component({
  selector: 'app-courses-preview-classes',
  templateUrl: './courses-preview-classes.component.html',
})
export class CoursesPreviewClassesComponent implements OnInit, OnDestroy {
  @ViewChild('classes') classes: AdvancedTableComponent;
  @ViewChild('search') search: AdvancedSearchFormComponent;
  public recordsTotal: any;
  private subscription: Subscription;
  private fragmentSubscription: Subscription;
  public course: any;

  constructor(private _activatedRoute: ActivatedRoute,
    private _context: AngularDataContext,
    private _activatedTable: ActivatedTableService,
    private _resolver: TableConfigurationResolver,
    private _searchResolver: SearchConfigurationResolver) { }

  async ngOnInit() {
    this.subscription = combineLatest(
        this._activatedRoute.params,
        this._resolver.get('CourseClasses', 'index'),
        this._searchResolver.get('CourseClasses', 'index')
      ).pipe(
        map(([params, tableConfiguration, searchConfiguration]) => ({params, tableConfiguration, searchConfiguration}))
      ).subscribe(async (results) => {
      this.course = results.params.id;
      this._activatedTable.activeTable = this.classes;
      this.classes.query = this._context.model('CourseClasses')
        .where('course').equal(results.params.id)
        .expand('course')
        .orderByDescending('year')
        .thenBy('period')
        .prepare();
      this.classes.config = AdvancedTableConfiguration.cast(results.tableConfiguration);
      this.classes.fetch();

      if (results.searchConfiguration) {
        this.search.form =  Object.assign(results.searchConfiguration, { course: this.course });
        this.search.ngOnInit();
      }

    });

    this.fragmentSubscription = this._activatedRoute.fragment.subscribe(fragment => {
      if (fragment && fragment === 'reload') {
        this.classes.fetch(true);
      }
    });
  }

  onDataLoad(data: AdvancedTableDataResult) {
    this.recordsTotal = data.recordsTotal;
  }

  ngOnDestroy(): void {
    if (this.subscription) {
      this.subscription.unsubscribe();
    }
    if (this.fragmentSubscription) {
      this.fragmentSubscription.unsubscribe();
    }
  }
}
